/*
*/

#include <Wire.h>
byte sensor[12];

void setup() {
  //start serial connection
  Serial.begin(9600);
  //configure pin 2 as an input and enable the internal pull-up resistor
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  pinMode(6, INPUT_PULLUP);
  pinMode(7, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
  pinMode(9, INPUT_PULLUP);
  pinMode(10, INPUT_PULLUP);
  pinMode(A0, INPUT_PULLUP);
  pinMode(A1, INPUT_PULLUP);
  pinMode(A2, INPUT_PULLUP);
  Wire.begin(8);                // join i2c bus with address #8
  Wire.onRequest(requestEvent); // register event
}

void loop() {
  //read the pushbutton value into a variable
  sensor[0] = !digitalRead(2);
  sensor[1] = !digitalRead(3);
  sensor[2] = !digitalRead(4);
  sensor[3] = !digitalRead(5);
  sensor[4] = !digitalRead(6);
  sensor[5] = !digitalRead(7);
  sensor[6] = !digitalRead(8);
  sensor[7] = !digitalRead(9);
  sensor[8] = !digitalRead(10);
  sensor[9] = !digitalRead(A0);
  sensor[10] = !digitalRead(A1);
  sensor[11] = !digitalRead(A2);


  //print out the value of the pushbutton
  for (byte i = 0; i < 12; i++)
  {
    output[i * 2] = sensor[i];
    Serial.print(sensor[i]);
    Serial.print(" ");

  }
  Serial.println();

}



void requestEvent()
{
  for (byte i = 0; i < 12; i++)
  {
    Wire.write(sensor[i]);
  }
}
